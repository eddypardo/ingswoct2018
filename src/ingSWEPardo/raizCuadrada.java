package ingSWEPardo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * Programa: Calcular la raiz cuadrada de un numero mediante el m�todo de
 * NewtonMethod
 *
 * @author Eddy Pardo
 * @version 1.0
 */

public class raizCuadrada {
	static PrintStream print;

	public static void main(String[] args) {
		try {
			print = new PrintStream(new File("EddyPardo.txt"));
			crearArchivo("********** Calcular la raiz cuadrada - M�todo NewtonMethod********* ");
			crearArchivo("Ingrese el n�mero: ");
			@SuppressWarnings("resource")
			Scanner input = new Scanner(System.in);
			double number;
			double sqrtValue = 1;
			double quotient;
			double average;
			int count = 0;
			do {// Inicio do While
				number = input.nextDouble();
				crearArchivo(String.valueOf(number));
				if (number > 0) {
					while (count < 5) {
						quotient = number / sqrtValue;
						average = (sqrtValue + quotient) / 2;
						sqrtValue = average;
						count++;
						crearArchivo("raiz cuadrada: " + sqrtValue);
					}
				} else {
					crearArchivo("ingrese otro numero");
					input.nextLine();
				}
			} while (number <= 0); // fin de do...while
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	static void crearArchivo(String mensaje) {
		PrintStream console = System.out;
		System.setOut(print);
		System.out.println(mensaje);
		System.setOut(console);
		System.out.println(mensaje);
	}
}
